from flask import Flask, request
from skimage import measure
from io import StringIO
import tensorflow as tf
import numpy as np
import nibabel as nib
import cv2 as cv
import tqdm
import os
import time
import load
import matplotlib.pyplot as plt
import threading
import random

"""
==== Globals ====
"""


IMG_SIZE = 512
SKIP_PIC = np.zeros((512, 512))  # when generating object, skip blank images
model = load.init()

predicting_threads = {} #threads currently running
app = Flask(__name__)  # Initalise Flask app

"""
==== Classes ====
"""

class PredictingThread(threading.Thread):
    def __init__(self, path):
        self.progress = 0
        self.progress_message = "Prediction Started"
        self.path = path
        super().__init__()

    def run(self):
        # Predition...
        self.progress = 0
        self.progress_message = "Loading CT Scan"
        cts = nib.load(self.path) # load nii scan from path
        print("Loaded CT scans.")
        self.progress += 5
        self.progress_message = "OK. Preprocessing."

        cropped_imgs = pre_process(cts)
        print("Cropped images.")
        self.progress += 5
        self.progress_message = "OK. Converting to tensor."

        cropped_imgs = tf.convert_to_tensor(cropped_imgs)
        print("Converted to tensor.")
        self.progress += 10
        self.progress_message = "OK. Making prediction."

        cropped_imgs = cropped_imgs / 255 # scale pixels form 0 -> 255 to 0 -> 1

        lung_masks, infec_masks = model.predict(cropped_imgs)
        print("Predicted.")
        self.progress += 40
        self.progress_message = "OK. Generating Mesh."
        post_lung_masks = post_process(lung_masks)
        path = generate_model(post_lung_masks) # return the name of the saved file
        self.progress += 40
        self.progress_message = path


"""
==== Helper Functions ====
"""
clahe = cv.createCLAHE(clipLimit=3.0)  # used to improve contrast in images


def clahe_enhancer(img, clahe, axes):
    """Contract Limited Adaptive Histogram Equalizer"""
    img = np.uint8(img * 255)
    clahe_img = clahe.apply(img)

    return clahe_img


def pre_process(cts):
    cts_all = []
    # slices = cts.shape[2]

    arr = cts.get_fdata()  # all the axises
    arr = np.rot90(np.array(arr))  # rotating every axis 90 deg
    # arr = arr[:, :, round(slices * 0.2) : round(slices * 0.8)]
    arr = np.reshape(np.rollaxis(arr, 2), (arr.shape[2], arr.shape[0], arr.shape[1], 1))

    for ii in range(arr.shape[0]):
        img = arr[ii]
        xmax, xmin = img.max(), img.min()
        img = (img - xmin) / (xmax - xmin)

        img = clahe_enhancer(img, clahe, [])

        img = cv.resize(img, dsize=(100, 100), interpolation=cv.INTER_AREA)
        img = np.reshape(img, (100, 100, 1))

        cts_all.append(img)

    return cts_all


def post_process(stack):
    processed_stack = []

    for image in stack:
        # scale image back from 0 -> 1 to 0 -> 255
        # cast to int because px is often a float
        image = np.array([np.array([int(255 * px) for px in row]) for row in image])

        image = np.uint8(image)  # need to convert for Canny detection

        # do some edge detection
        # the thresholds might want to played about with
        image = cv.Canny(image, threshold1=100, threshold2=100)

        processed_stack.append(image)

    return np.array(processed_stack)


def combine(l, i):
    """combined the lung and infection masks.
    doesn't matter if which arguement is which set of masks
    """

    # pre compute lengths for clarity
    images = len(i)
    rows = len(i[0])
    pxs = len(i[0][0])

    # make a blank image the same shape as the given arguement
    combined = np.zeros(l.shape, dtype="uint8")

    for image in range(images):
        for row in range(rows):
            for px in range(pxs):
                # l * i should only contain 0 or 255
                # Python evaluates everything but 0 as True, so we can do funky stuff like this
                if l[image][row][px] or i[image][row][px]:
                    combined[image][row][px] = 255

    return combined


def generate_model(imgs):
    """Generate Wavefront obj from stack of images. \n
    Currently, only works with one axis of images.

      +------+
     /|     /|
    +-|----+ | z
    | |    | |
    | +----|-+
    |/     |/ y
    +------+
        x

    Think of the CT scan as a "stack" of images.
    The x, y axises are relevant to each slice of the stack.
    The z axis corresponds to the slice in the stack.

    If we find a pixel in a slice that we like (in this
    case a white one), we can use infer the axis data
    described above to put a vertex at that point.

    This only get's us half of the way, faces are also
    needed to create an object. For this we use an algorithm
    called marching cubes. Here, we use the scipy implementation
    as it binds to cpp functions and thus is much faster than
    any Python implmentation.

    From that algorithm, we get a set of verticies and faces.
    These must be converted into a file format that Unity can read.
    Wavefront obj files are chosen for their simplicity.

    A vertex is described as follows:
    v x y z (where x y z are the coordinates of the vertex).

    A face is described as follows:
    f v1 v2 v3 ... vn (where vn is the line number of the vertex)
    """

    # pre-define the size of our point cloud
    # this is much faster than constantly resizes a pythonn array
    verts = np.zeros((imgs.shape[0], imgs.shape[1], imgs.shape[2]), dtype=bool)
    seen = set()

    for z in tqdm.tqdm(range(len(imgs))):
        img_data = imgs[z]

        # go through every pixel
        # if you find a white pixel add a vertex at that point
        for x, row in enumerate(img_data):
            for y, px in enumerate(row):
                seen.add(px)
                if px == 255:
                    verts[z][y][x] = True
    print(seen)
    print("OK. Marching cubes...")
    vertices, faces, _, _ = measure.marching_cubes(
        verts
    )  # this is how we generate the mesh

    # use StringIO buffers because they're much faster than concatenating
    # concatenating: hours, StringIO: seconds
    vert_buff = StringIO()
    face_buff = StringIO("usemtl Default\n")

    # current verticies generate from bottom left to top,
    # offset it so that the origin is in the center of the object
    offsets = (imgs.shape[0] / 2, imgs.shape[1] / 2, imgs.shape[2] / 2)
    # psudeo-scaling. nifti data should have  spatial data in but our dataset doesn't
    scaling = imgs.shape[1] / imgs.shape[0]

    print("Building vertex string.")
    for vert in vertices:
        vert_buff.write(
            f"v {vert[2] - offsets[2]} {(vert[0]- offsets[0])* scaling} {vert[1] - offsets[1]}\n"
        )

    print("Building face string.")
    for face in faces:
        # +1 the index of the array because line counting starts at 1
        face_buff.write(f"f {face[0] + 1} {face[1] + 1} {face[2] + 1}\n")

    obj_file = vert_buff.getvalue() + face_buff.getvalue()
    if not os.path.exists("saved_objects"):
        os.makedirs("saved_objects")
    file_name = fr"saved_objects\{time.strftime('%d-%m-%Y~%H_%M_%S')}.obj"

    f = open(file_name, "w")
    f.write(obj_file)
    f.close()

    print("OK.")
    # might want to change where the generated obj is stored
    # current just dumps it in the currently working directory
    return os.path.join(os.getcwd(), file_name)


def compare_actual_and_predicted(img_num, num_pix, X_test, yl_pred, yi_pred):

    fig = plt.figure(figsize=(11, 7))

    plt.subplot(1, 3, 1)
    plt.imshow(tf.reshape(X_test[img_num], [num_pix, num_pix]))
    plt.title("CT image")

    plt.subplot(1, 3, 2)
    plt.imshow(tf.reshape(yl_pred[img_num], [num_pix, num_pix]), cmap="bone")
    plt.title("predicted lung mask")

    plt.subplot(1, 3, 3)
    plt.imshow(tf.reshape(yi_pred[img_num], [num_pix, num_pix]), cmap="bone")
    plt.title("predicted infection mask")

    # plt.show()
    plt.savefig(
        rf"G:\lung_dataset\predicted_lung_masks\coronavirus_001\{img_num}.png",
        bbox_inches="tight",
    )

    plt.close(fig)


"""
==== API Endpoints ====
"""
@app.route("/predict", methods=["PUT"])
def predictPath():
    if request.method == "PUT":
        """Creates thread for the specified prediction and returns thread ID"""
        path = request.data.decode("utf-8") # get path
        global predicting_threads

        thread_id = random.randint(0, 10000)
        predicting_threads[thread_id] = PredictingThread(path)
        predicting_threads[thread_id].start() 

        return str(thread_id)

    return "error"

@app.route('/progress/<int:thread_id>')
def progressValue(thread_id):
    """Returns progress value of the specified thread ID"""
    global predicting_threads
    print(predicting_threads[thread_id].progress)
    return str(predicting_threads[thread_id].progress)

@app.route('/message/<int:thread_id>')
def progressMessage(thread_id):
    """Returns progress message of the specified thread ID"""
    global predicting_threads
    return str(predicting_threads[thread_id].progress_message)

if __name__ == "__main__":
    app.run(debug=False, port=9090)