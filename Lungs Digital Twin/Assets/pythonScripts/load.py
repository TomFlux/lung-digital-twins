# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 16:53:54 2021

@author: sanak
"""

from keras import backend as K
from keras import losses
from keras.models import load_model, model_from_json


def bce_dice_loss(y_true, y_pred):  # Binary Cross-Entropy
    loss = 0.5 * losses.binary_crossentropy(y_true, y_pred) + 0.5 * dice_loss(
        y_true, y_pred
    )
    return loss


def dice_coeff(y_true, y_pred, smooth=1):
    intersection = K.sum(y_true * y_pred, axis=[1, 2, 3])
    union = K.sum(y_true, axis=[1, 2, 3]) + K.sum(y_pred, axis=[1, 2, 3])
    dice = K.mean((2.0 * intersection + smooth) / (union + smooth), axis=0)
    return dice


def dice_loss(y_true, y_pred):
    loss = 1 - dice_coeff(y_true, y_pred)
    return loss


def weighted_bce_loss(y_true, y_pred, weight):
    epsilon = 1e-7
    y_pred = K.clip(y_pred, epsilon, 1.0 - epsilon)
    logit_y_pred = K.log(y_pred / (1.0 - y_pred))
    loss = weight * (
        logit_y_pred * (1.0 - y_true)
        + K.log(1.0 + K.exp(-K.abs(logit_y_pred)))
        + K.maximum(-logit_y_pred, 0.0)
    )
    return K.sum(loss) / K.sum(weight)


def weighted_dice_loss(y_true, y_pred, weight):
    smooth = 1.0
    w, m1, m2 = weight, y_true, y_pred
    intersection = m1 * m2
    score = (2.0 * K.sum(w * intersection) + smooth) / (
        K.sum(w * m1) + K.sum(w * m2) + smooth
    )
    loss = 1.0 - K.sum(score)
    return loss


def weighted_bce_dice_loss(y_true, y_pred):
    y_true = K.cast(y_true, "float32")
    y_pred = K.cast(y_pred, "float32")
    averaged_mask = K.pool2d(
        y_true, pool_size=(50, 50), strides=(1, 1), padding="same", pool_mode="avg"
    )
    weight = K.ones_like(averaged_mask)
    w0 = K.sum(weight)
    weight = 5.0 * K.exp(-5.0 * K.abs(averaged_mask - 0.5))
    w1 = K.sum(weight)
    weight *= w0 / w1
    loss = 0.5 * weighted_bce_loss(y_true, y_pred, weight) + 0.5 * dice_loss(
        y_true, y_pred
    )
    return loss


def init():

    model = load_model(
        "pythonImplem/trained_models/26-03_400e_nc_a.h5",
        custom_objects={
            "weighted_bce_dice_loss": weighted_bce_dice_loss,
            "dice_coeff": dice_coeff
            # "weighted_dice_coeff": weighted_dice_coeff,
        },
        compile=True,
    )

    return model


def old_init():
    json_file = open("ctsmodel-lungs-infects.json", "r")
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("ctsmodel-lungs-infect-seg.hdf5")
    # json_file = open('ctsmodel-lungs-infects.json','r')
    # loaded_model_json = json_file.read()
    # json_file.close()
    # loaded_model = model_from_json(loaded_model_json)
    # load woeights into new model
    # loaded_model.load_weights("ctsmodel-lungs-infect-seg-2.h5")
    # print("Loaded Model from disk")
    # optim = keras.optimizers.Adam(lr=1e-3, beta_1=0.9, beta_2=0.99)
    # loss_dict = {'lung_output': bce_dice_loss, 'infect_output': bce_dice_loss}
    # loss_weight_dict = {'lung_output': 1.0, 'infect_output': 1.0}
    # compile and evaluate loaded model
    # loaded_model.compile(optimizer=optim, loss=loss_dict, loss_weights=loss_weight_dict, metrics=[dice_coeff])
    # loaded_model.compile(optimizer=Adam(lr = 0.0005), loss=bce_dice_loss, metrics=[sm.metrics.IOUScore(threshold=0.55)])
    # loss,accuracy = model.evaluate(X_test,y_test)
    # print('loss:', loss)
    # print('accuracy:', accuracy)
    # graph = tf.get_default_graph()

    return loaded_model