import uuid


from flask import Flask, request, jsonify
from flask_mysqldb import MySQL, MySQLdb
#install these to be able to test this
#the ip you can see in the browser is:  127.0.0.1:5000 in the format of json

app = Flask(__name__)
app.secret_key = uuid.uuid4().hex
app.config['MYSQL_HOST'] = 'db.tomflux.xyz'
app.config['MYSQL_USER'] = 'group_twenty'
app.config['MYSQL_PASSWORD'] = 'CO2201'
app.config['MYSQL_DB'] = 'testDB'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

mysql = MySQL(app)

@app.route('/api/exData', methods=['GET','PUT'])
def data_db():
    if request.method == "GET":
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute("SELECT * FROM models WHERE id = 2")
        selected = cursor.fetchall()
        for result in selected:
            content = {'id':result['id'],'model':str(result['model'],"utf-8"),'scan_id':result['scan_id']}
        return jsonify(content)
    elif request.method == "PUT":
        data = request.get_json()
        file = data['objfile']
        fileInBinary = ''.join(format(ord(i), '08b') for i in file)
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        id = 2
        cursor.execute("INSERT INTO modelsNew VALUES (%s, %s)", (id, fileInBinary))
        content = {'objfile':file}
        return jsonify(content)
    else:
        content = {'error':'some sort of error'}
        return jsonify(content)



# @app.route('/api/getData', methods=['PUT'])
# def get_data_from_unity():
#     if request.method == "PUT":
#         data = request.get_json()
#         file = data['objfile']
#         fileInBinary = ''.join(format(ord(i), '08b') for i in file)
#         cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
#         id = 2
#         cursor.execute("INSERT INTO modelsNew VALUES (%s, %s)", (id, fileInBinary))
#         #return jsonify(file)
#     return "error"


if __name__ == '__main__':
    app.run(debug=True)
