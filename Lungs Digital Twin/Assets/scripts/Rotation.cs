﻿using UnityEngine;
using System.Collections;
using System;

public class Rotation : MonoBehaviour
{
    Boolean selected = false;
    Vector3 mPrevPos = Vector3.zero;
    Vector3 mPosDelta = Vector3.zero;
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            selected = true;
        }
    }
    
    private void Update()
    {
        if (selected == true)
        {
            mPosDelta = Input.mousePosition - mPrevPos;
            if (Vector3.Dot(transform.up, -Vector3.up) >= 0)
            {
                transform.Rotate(transform.up, Vector3.Dot(mPosDelta, Camera.main.transform.right), Space.World);
            }
            else
            {
                transform.Rotate(transform.up, -Vector3.Dot(mPosDelta, Camera.main.transform.right), Space.World);
            }
            transform.Rotate(Camera.main.transform.right, Vector3.Dot(mPosDelta, Camera.main.transform.up), Space.World);
        }
        mPrevPos = Input.mousePosition;
        if (Input.GetMouseButtonUp(1))
        {
            selected = false;
        }
    }
}