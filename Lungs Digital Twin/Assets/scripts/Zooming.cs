﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zooming : MonoBehaviour
{
    public Camera cam;
    public float targetZoom;
    [SerializeField] private float zoomFactor = 40f;
    private float zoomLerpSpeed = 10;
    [SerializeField] private float min = 10f;
    [SerializeField] private float max = 500f;
    private void Start()
    {
        cam = Camera.main;
        targetZoom = cam.orthographicSize;
    }
    void Update()
    {
        float scrollData;
        scrollData = Input.GetAxis("Mouse ScrollWheel");

        targetZoom -= scrollData * zoomFactor;
        targetZoom = Mathf.Clamp(targetZoom, min, max);
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, targetZoom, Time.deltaTime * zoomLerpSpeed);
    }
}
