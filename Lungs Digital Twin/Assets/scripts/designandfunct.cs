
using AnotherFileBrowser.Windows;
using UnityEngine;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;
using System.Collections;
using Dummiesman;




public class designandfunct : MonoBehaviour

{
    string urlApi = "http://127.0.0.1:9090/predict";
    string urlProgress = "http://127.0.0.1:9090/progress/";
    string urlMessage = "http://127.0.0.1:9090/message/";
    public string ThePath;
    System.Diagnostics.Process processToKill;
    Boolean ImportScanOn = true;
    Boolean ImportObjOn = true;
    public Button ImportScan;
    public Button ImportObj;
    public GameObject loadedObj;
    public GameObject model;
    public GameObject panel;
    public GameObject MainMenu;
    public GameObject ViewMenu;
    public GameObject LoadingScreen;
    public Slider slider;
    public Text loadingpercentage;
    public Text MessageOnLoad;
    private int progress;
    private string ThreadID;
    private Boolean CheckForScan = false;
    



    // Start is called before the first frame update
    void Start()
    {

        Debug.Log("****************************            " + Application.dataPath + "         *****************************************");
        //StartCoroutine(getThreadID(urlApi));

    }


    //Runs python script hidden so the lung model could be generated and shown within Unity
    static System.Diagnostics.Process runPythonCode(string apiName)
    {
        Debug.Log("Begin");
        System.Diagnostics.Process process = new System.Diagnostics.Process();
        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
        startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
        startInfo.FileName = "cmd.exe";
        Debug.Log(Application.dataPath);
        //startInfo.Arguments = "/C python Assets/pythonScripts/" + apiName + "& pause";
        startInfo.Arguments = "/C env\\Scripts\\python.exe pythonImplem\\" + apiName + "& pause";
        
        process.StartInfo = startInfo;
        process.Start();
        Debug.Log(process.Id);
        Debug.Log("end");
        return process;

    }
    /*function used to notify tester that application has quit*/
    void OnApplicationQuit()
    {
        Debug.Log("Application has quit");
        applicationDestroy();
    }

    public void applicationDestroy()
    {



        //Finds the process that runs in the background and closes it so the resources arent used
        //processToKill.Kill();
        foreach (System.Diagnostics.Process p in System.Diagnostics.Process.GetProcessesByName("python"))
        {
            //checks if any processes with name Python exists
            if (System.Diagnostics.Process.GetProcessesByName("python").Length > 0)
            {
                //Kill an instance that started the cmd
                processToKill.Kill();
                p.CloseMainWindow();
            }

        }
    }

    public void ImportScanButton()
    {
   
        processToKill = runPythonCode("app.py");
        StartCoroutine(SendPathOfCTScanToML(urlApi));
        Debug.Log("|." + ThePath + ".|");
        if (ThePath == "")
        {
                
            applicationDestroy();
        }
        else
        {
            Debug.Log(ThePath);
            StartCoroutine(StatusBar());
            LoadingScreen.SetActive(true);
            MainMenu.SetActive(false);
            panel.gameObject.SetActive(true);
        }
    
    }
    public void ImportObjButton()
    {

        LoadObjWithExplorer();
        if (ThePath != "")
        {
            panel.gameObject.SetActive(false);
        }
    }

    public void DestroyButton()
    {
        panel.gameObject.SetActive(true);
        progress = 0;
        slider.value = 0;
        MessageOnLoad.text = "";
        GameObject.Find("Main Camera").GetComponent<Zooming>().targetZoom = 120;
        GameObject.Find("Main Camera").GetComponent<Zooming>().cam.orthographicSize = 120;

        if (loadedObj != null)
        {
            DestroyThis(loadedObj);
            ThePath = "";
            applicationDestroy();

        }
        else
        {
            ThePath = "";
            applicationDestroy();
        }

    }
    //The quit button
    public void QuitButton()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    //Allows you to change the state of a button to addapt to the environment
    public void CheckButtonStatus()
    {
        if (ImportScanOn == true)
        {
            ImportScan.interactable = true;
        }
        if (ImportScanOn == false)
        {
            ImportScan.interactable = false;
        }
        if (ImportObjOn == true)
        {
            ImportObj.interactable = true;
        }
        if (ImportObjOn == false)
        {
            ImportObj.interactable = false;
        }
    }

    //This method deletes the created obj from the scene
    public void DestroyThis(GameObject theObj)
    {
        Destroy(theObj);
    }

    IEnumerator SendPathOfCTScanToML(string url)
    {   
        Debug.Log("SendPathOfCTScanTOML");
        OpenFileBrowser("nii");
        if (ThePath == "")
        {
            Debug.Log("Scan path is not selected");
        }
        else
        {
            yield return new WaitForSeconds(10);
            CheckForScan = true;
            UnityWebRequest request = UnityWebRequest.Put(url, ThePath);
            request.SetRequestHeader("Content-Type", "application/text");
            yield return request.Send();

            ThreadID = request.downloadHandler.text;
            Debug.Log("Status Code: " + request.responseCode);
            Debug.Log("*******"+ThreadID+"********");
            
        }
    }


    IEnumerator catchProgress(string url)
    {
        Debug.Log("Progress");
        
        UnityWebRequest progressRequest = UnityWebRequest.Get(url);
        progressRequest.SetRequestHeader("Content-Type", "application/text");
        yield return progressRequest.Send();
        Debug.Log(progressRequest.downloadHandler.text);

        progress = Int32.Parse(progressRequest.downloadHandler.text);
        slider.value = progress;
    }

    IEnumerator catchMessage(string url)
    {
        
        UnityWebRequest messageRequest = UnityWebRequest.Get(url);
        messageRequest.SetRequestHeader("Content-Type", "application/text"); 
        yield return messageRequest.Send();
        string Message = messageRequest.downloadHandler.text;

        if (!Message.Contains("!"))
        {
            MessageOnLoad.text = Message;
        }
        else 
        {
            MessageOnLoad.text = "Loading";
        }
        
    }
    IEnumerator StatusBar() 
    {
        while (true) {
            if (progress != 100)
            {
                Debug.Log("STATUSBAR" + ThreadID);
                
                StartCoroutine(catchProgress(urlProgress+ThreadID));
                StartCoroutine(catchMessage(urlMessage+ThreadID));

            }
            else
            {
                Debug.Log("***About to Break***");
                break;
            }
            
            yield return new WaitForSeconds(0.5f);
        }
        LoadObj(MessageOnLoad.text);
    }


    /*This method simply return the path for All types of files after browsing*/
    public string OpenFileBrowser(string filetype)
    {   
        var bProp = new BrowserProperties();
        bProp.filter = "File Type (*." + filetype + ") | *." + filetype;
        bProp.filterIndex = 0;
        //bProp.initialDir = "C:\\";
        //bProp.initialDir = Application.dataPath + "C:/saved_objects";
        new FileBrowser().OpenFileBrowser(bProp, path =>
        {
            ThePath = LoadPath(path);
        });
        return null;
    }
    public string LoadPath(string TheScanPath)
    {
        return TheScanPath;
    }

    //Loads a previously saved model on unity screen
    void LoadObjWithExplorer()
    {
        OpenFileBrowser("obj");
        Debug.Log(ThePath);
        if (ThePath != "")
        {
            LoadObj(ThePath);
        }
        else
        {
            Debug.Log("NO model selected");
        }
    }

    void LoadObj(string path)
    {
        
        loadedObj = new OBJLoader().Load(path);
        loadedObj.transform.position = new Vector3(55, 25, 500);
        loadedObj.transform.localScale = new Vector3(-2, 2, 2);
        loadedObj.transform.Rotate(0.0f, 90.0f, 0.0f, Space.World);
        GameObject Child = loadedObj.transform.GetChild(0).gameObject;
        Debug.Log(Child.name);
        Child.AddComponent<BoxCollider>();
        Child.AddComponent<Drag>();
        Child.AddComponent<Rotation>();
        
    }

    void ProgressHandler()
    {
        loadingpercentage.text = slider.value + "%";
        CheckProgress();
    }
    void CheckProgress()
    {
        if (slider.value == 100)
        {
            LoadingScreen.SetActive(false);
            ViewMenu.SetActive(true);
            panel.SetActive(false);

        }
    }

    void Update()
      {

        if (CheckForScan == true)

        {
            ProgressHandler();
        }

        CheckButtonStatus();
    }
}