# Lung Digital Twins

This project was created as part of a group during my second year Computer Science BSc. studies.

My main area of development was generating the digital twins from the received chest scan.

## Installation

**Requirements:**
- Windows 10.
- [Python](https://www.python.org/ftp/python/3.7.9/python-3.7.9-amd64.exe) 3.6.x -> 3.8.x (3.7.9 is known to work)

Please make sure you have the correct version of python installed. Not only does the version have to be correct, but how Python has been installed is also important. Windows 10 often comes packaged with Python, which Tensorflow does not like. To check if you have the correct version of Python installed simply run `check_install.py`.

If the script tells you things are wrong, there are two options:

1. Play around with your PATH so that the command `python` runs the correct version of Python.
2. Uninstall all versions of Python 3, this can be done from the control pannel. Then install python from the link provided above.

Once Python has been set up correctly, run `Builds/install.cmd`. This does five simple steps:

1. Install the virtual environment package.
2. Creates a new virtual environment in the root of the project.
3. Enters into the new virtual environment.
4. Installs all the requirements.
5. Leaves the virtual environment.

**Optional**

Tensorflow is used as our machine learning backend. By default, this runs on the CPU; however, a GPU can be used for better performance although is fiddly to setup. The offical installation can be found [here](https://www.tensorflow.org/install/gpu?hl=ur).


## How to Run

Once you have completed the above installation steps, you can simply run the "Lung Digital Twin.exe" inside of the Builds folder.
