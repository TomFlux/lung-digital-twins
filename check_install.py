import os
import sys

if r"AppData\Local\Programs\Python" not in os.path.dirname(sys.executable):
    print(
        f"Python is not installed in the correct place.\nThis instance of Python comes from: {os.path.dirname(sys.executable)}"
    )
else:
    print("All good.")
